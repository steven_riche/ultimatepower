# Ultimate Power

In your search for the Ultimate Power, you have learned of an ancient artifact that alone may harness this destructive force.

This exceedingly rare artifact has only been known to be available from one source.

- Clone this repo locally. Open a terminal window on your machine, and type the following:

```
git clone https://steven_riche@bitbucket.org/steven_riche/ultimatepower.git
```

This will create the folder `ultimatepower`

- Unzip `web.zip`

- Open `index.html` in a web browser

From here, you must find your own way to obtain this artifact.
